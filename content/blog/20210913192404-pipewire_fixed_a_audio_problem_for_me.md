+++
title = "Pipewire fixed my USB speaker"
author = ["A.I."]
lastmod = 2021-09-13T20:03:55+08:00
slug = "pipewire_fixed_my_usb_speaker"
categories = ["linux", "gentoo"]
draft = false
+++

I only notice [PipeWire](https://pipewire.org) from a ~~hot~~ [hacker news headline](https://news.ycombinator.com/item?id=28490078) this morning, the top comment mentioned that it can replace PulseAudio caught my attention.

I had some struggles with PulseAudio before. The biggest one is that my USB speaker (Sony SRS-HG10) stopped working since I updated my kernel to 5.12. I have tried a lot of fixes, tweaking kernel configs, clean PulseAudio caches, etc. Finally, I gave up and wore a headphone instead.

After reading the [ArchWiki page](https://wiki.archlinux.org/title/PipeWire) and [Gentoo Wiki page](https://wiki.gentoo.org/wiki/PipeWire) about PipeWire I find it's quite easy to replace PulseAudio with it.

-   Stop PulseAudio from auto spawning.
    Edit `/etc/pulse/client.conf` and un-comment line `autospawn = no`

-   start PipeWire when X starts.
    It's possible to add it to any X start script like `.xinirc` or `.xprofile`, I did it in my `i3/config`

    ```shell
    exec --no-startup-id pipewire
    ```

That's it. I restarted i3, opened up a Youtube video. BOOOOOM, everything works.

Just to be sure.

```shell
ps -aux | grep pulse
# make sure no PulseAudio process running
```

```shell
pactl info | grep "Server Name"
# : Server Name: PulseAudio (on PipeWire 0.3.30)
```

Open `pavucontrol` and switch to USB speaker, it works too, congratulations (to me)!

There is also [a ticket](https://bugs.gentoo.org/744622) in Gentoo bug tracker discussing how to avoid installing PulseAudio entirely. I'm looking forward to it.
