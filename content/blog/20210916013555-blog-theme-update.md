+++
title = "博客主题更新"
author = ["A.I."]
lastmod = 2021-10-05T14:11:40+08:00
slug = "blog-theme-update"
categories = ["life", "blog", "Chinese"]
draft = false
+++

最近的几篇博客我都在尝试使用英文写，其一是因为大部分的工作、非工作文档需要使用英文编写，其二是因为编程相关的术语较多，而我一向不喜欢这些术语的中文翻译。所以权当练习（？）。

之前为了这个还特意把博客分开了中文页面和英文页面，其实是瞎用了 Hugo 的“翻译”功能，并不是非常合适，所以决定更新一下主题，顺便改几个 Bug(?)。

博客主题 Repo: <https://github.com/merrickluo/hugo-tailwind-journal>


## 中英文 tag {#中英文-tag}

虽然自己的英文写作水平比较低，不过还是希望坚持一下多写一点。由于不再使用 Hugo 的翻译功能，所以给中英文的博客分别打上了标签。


## pangu.js {#pangu-dot-js}

我在 Emacs 里一直在使用 [pangu-spacing](https://github.com/coldnew/pangu-spacing)， 这次给博客也加上了 [pangu.js](https://github.com/vinta/pangu.js/)。

不过感觉可能作用有限了，由于在 Org-mode 里不中英文不加上空格，spell-check 会变得相当混乱，所以我现在都把 `pangu-spacing-real-insert-separtor` 又打开了，以后的博客内容应该都会自带空格。


## no-script {#no-script}

之前给博客主题添加了一个简单的 Light/Dark Mode 切换按钮，用到了几行 JavaScript，这次在偶然关掉浏览器 JavaScript 的情况下打开之后发现就定在 Dark Mode 了，狠狠的伤害了我自己的眼睛。并且右上角的切换按钮在没有 JavaScript 的情况下也毫无作用。

更新后在没有 JavaScript 的情况下，会使用浏览器的 `prefers-color-scheme` 值设定 Light/Dark Mode，并不再显示切换按钮。


## <span class="org-todo todo TODO">TODO</span> Live Section {#live-section}

下一步准备给博客增加一个 "Live Section"，大概目的是用来放新弄的 Literate Doom Emacs 配置，随时更新，并且发布出来也会让我给配置补充文档增加点动力，参考     [tecosaur 的配置](https://tecosaur.github.io/emacs-config/config.html)。

或许还有一些其他用处，比如用来更新自己的读书笔记？（说得好像会读书）。
