+++
title = "Speedup aur Package Install by 1000%"
author = ["A.I."]
lastmod = 2020-04-30T18:05:40+08:00
slug = "speedup-aur-package-install-by-1000"
categories = ["archlinux", "English"]
draft = false
weight = 2001
+++

And... of course this is a click-bait title, This will only speedup the compress time when packaging the package, not the whole build time 🙂️.

One night in a peaceful Telegram group, a member starts complaining about aur packages install is too slow, and the `xz` process is only consuming one core. This leads to a meaningful discussion about how aur works and how to speed up the build, but I'll skip the first part by now, if you are interested, just follow [Makepkg](https://wiki.archlinux.org/index.php/Makepkg) page on Arch Wiki.

In short, aur is using makepkg to build the package and then install it with pacman, and there is a `/etc/makepkg.conf` we can tweak to make it works better.

## zstd {#zstd}

Another thing pop up in the discussion is [zstd](https://github.com/facebook/zstd), pacman [switches to zstd from xz](https://www.archlinux.org/news/now-using-zstandard-instead-of-xz-for-package-compression/) on Friday, Dec 27, 2019, and claims it had a 1300% speedup. Since we can also use it in makepkg now, so it's a great time to switch.
Just change one line in `/etc/makepkg.conf` to make it use zstd.

```diff
- PKGEXT='.pkg.tar.xz'
+ PKGEXT='.pkg.tar.zst'
```

## Multithread {#multithread}

The main complain is `xz` is only using one thread when there are 8/16 available. It's because the commands is not setup to use multi-thread by default. we can change it as documented in the [Makepkg](https://wiki.archlinux.org/index.php/Makepkg) wiki page.
This change can change zstd to use multi-thread (similar to `xz`).

```diff
- COMPRESSZST=(zstd -c -z -q -)
+ COMPRESSZST=(zstd -c -z -q - --threads=0)
```

## Use tmpfs {#use-tmpfs}

If you have spare memory, you can also use it for speed up build. This is also documented in the [Makepkg](https://wiki.archlinux.org/index.php/Makepkg) wiki page.
Just uncomment one line in `/etc/makepkg.conf`

```diff
- #BUILDDIR=/tmp/makepkg
+ BUILDDIR=/tmp/makepkg
```

## Conclusion {#conclusion}

These steps should give you a 1000%+ speedup with aur package compressing(according to [Robin Broda](https://www.archlinux.org/news/now-using-zstandard-instead-of-xz-for-package-compression/)). I have so many times starring at aur package install stuck at `Compressing Package` step, but never thought of how to optimize it. #TodayILearned

## History {#history}

- 2020-04-30 add title explanation.
