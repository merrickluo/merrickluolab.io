+++
title = "Roam Capture Workflow in Doom Emacs"
author = ["A.I."]
date = 2020-04-23T20:08:11+08:00
publishDate = 2020-04-23T20:08:11+08:00
lastmod = 2020-04-30T00:08:43+08:00
slug = "roam-capture-workflow-in-doom-emacs"
categories = ["emacs", "English"]
draft = false
weight = 2007
+++

I don't take much notes before, until I recently discovered [org-roam](https://github.com/jethrokuan/org-roam), I tried to learn some note taking workflow in org-mode before, but none of them work out, it's hard to manage all things in a tree if you are not so organized :). org-roam's back link feature is much easier to work with, I can put random stuff in and some tags I think is good, and it's automatically organized like a wiki.

Anyway, I'm still learning and here is how I use it when I want to capture some web page into the roam graph.

## Setup Roam Protocol {#setup-roam-protocol}

org-roam [supports org-protocol](https://org-roam.readthedocs.io/en/latest/roam%5Fprotocol/), so you need to setup org-protocol and roam beforehand, luckily, [doom-emacs](https://github.com/hlissner/doom-emacs) setup this nicely for me, all I have to do is change the `$doomdir/init.el` like this:

```emacs-lisp
(org                ; organize your plain life in plain text
  +dragndrop         ; drag & drop files/images into org buffers
  +hugo              ; use Emacs for hugo blogging
  +roam
  +journal
  ;;+jupyter         ; ipython/jupyter support for babel
  ;;+pandoc          ; export-with-pandoc support
  ;;+pomodoro        ; be fruitful with the tomato technique
  ;;+present)          ; using org-mode for presentations
```

Of course, only the `roam` part is necessary.

And a desktop file `$HOME/.local/share/applications/org-capture.desktop` to handle `org-protocol://` links.

```nil
[Desktop Entry]
Name=Org Capture
GenericName=Text Editor
Comment=Edit text
MimeType=x-scheme-handler/org-protocol;
Exec=emacsclient -a emacs -c -F '((name . "doom-capture") (transient . t))' %u
Icon=emacs
Type=Application
Terminal=false
Categories=Development;TextEditor;
StartupWMClass=Emacs
Keywords=Text;Editor;
```

The name for the new frame is for identify in window manager, and the transient is for close the new frame after capture is done. Since I'm using awesome wm, so I didn't setup the frame size.

Then follow the guide in [org-roam documents](https://org-roam.readthedocs.io/en/latest/roam%5Fprotocol/), add a bookmarklet with following content:

```javascript
javascript: location.href =
  "org-protocol://roam-ref?template=r&ref=" +
  encodeURIComponent(location.href) +
  "&title=" +
  encodeURIComponent(document.title);
```

or if with Firefox and [tridactyl](https://github.com/tridactyl/tridactyl), add a shortcut in `$HOME/.tridactylrc`:

```vimrc
bind ;r js javascript:location.href = 'org-protocol:/roam-ref?template=r&ref=' + encodeURIComponent(location.href) + '&title=' + encodeURIComponent(document.title)
```

Now when I press ;r in any site, an emacs frame opens up, with title and `#ROAM_KEY` ready, I can just put my note and thoughts about it and hit C-c C-c.

## More space for notes {#more-space-for-notes}

roam-capture buffer opens in a popup buffer in doom emacs, by default it's too small for taking notes, so I added a popup rule to increase it.

in `$doomdir/config.el`

```emacs-lisp
(set-popup-rule! "^CAPTURE" :side 'bottom :size 0.90 :select t :ttl nil)
```

## Optimize for awesome wm {#optimize-for-awesome-wm}

As I said above, I'm using awesome wm, like many tiling wm user, I place my Emacs window in a specific tag, the first one, for me, and Firefox in a specific tag, and it's the second for me. But this is not very convenient for capture workflow, every time I press `;r` in Firefox, the emacs frame is opened in the first tag, so I need to switch to it and take notes, since the name of the capture frame is fixed `doom-capture`, so I just exclude it from the rule.

```lua
ruled.client.append_rule {
    id         = "emacs",
    rule   = {
        class = "Emacs",
    },
    except = { name = "doom-capture"},
    properties = {
        tag = '1'
    }
}
```

It's working for me pretty well, I take more notes in two weeks than last 4 years, and my tabs in Firefox is much less since I actually read them. Although it's not the only thing org-roam offers, but it's a good start.
